# vseqinfo(1)

## NAME

vseqinfo - obtain sequence information from index

## SYNOPSIS

*vseqinfo* indexname

## DESCRIPTION

*vseqinfo* echoes for each database sequence its length and its description.
The program has no options. It takes exactly one argument, namely the index
name. The output goes to standard output.

## SEE ALSO

vseqselect(1)
