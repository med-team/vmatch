# vsubseqselect(1)

## NAME

vsubseqselect - print selected subsequences from index

## SYNOPSIS

*vsubseqselect* [options] indexname

## DESCRIPTION

The program *vseqselect* selects subsequences from a given index and prints
them on standard output, either line by line or in FASTA format. The selection
can either be random or according to position ranges specified by the user.

Please refer to the manual for more detailed explanations.

## OPTIONS

*-minlength*::
  Specify the minimal length of the substrings to be selected.

*-maxlength* <length>::
  Specify the maximal length of the substrings to be selected.

*-snum* <n>::
  Specify the number of random substrings to be selected.

*-range* <pos> <pos>::
  Specify the first and last position of the substring to be selected.

*-seq* <length> <number> <pos>:: 
  Specify length, number, and relative position of the substring to be selected.

*-version*::
  Show the version of the Vmatch package

*-help*::
  Show help.
