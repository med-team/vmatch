# vseqselect(1)

## NAME

vseqselect - print selected sequences from index

## SYNOPSIS

*vseqselect* [options] indexname

## DESCRIPTION

The program *vseqselect* selects sequences from a given index and prints them
on standard output.

## OPTIONS

*-minlength*::
  Specify the minimal length of the sequences to be selected.

*-maxlength* <length>::
  Specify the maximal length of the sequences to be selected.

*-randomnum* <n>::
  Specify the number of random sequences to be selected.

*-randomlength* <length>::
  Specify the minimal total length of the random sequences to be selected.

*-seqnum* <filename>::
  Select the sequences with numbers given in filename.

*-version*::
  Show the version of the Vmatch package

*-help*::
  Show help.
